<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/

//Route::get('/{any}', 'VueController@index')->where('any', '.*');
Route::get('', function () { return redirect('/en'); });
Route::get('{locale}', 'WelcomeController@index');
Route::get('home/{locale}', 'HomeController@index');
Route::get('products/{locale}', 'ProductsController@index');
Route::get('products/{subcat}/{locale}', 'ProductsController@subprods');
Route::get('products/{subcat}/{subcat2}/{locale}', 'ProductsController@subprods2');
Route::get('products/{subcat}/{subcat2}/{subcat3}/{locale}', 'ProductsController@subprods3');
Route::get('products/{subcat}/{subcat2}/{subcat3}/{subcat4}/{locale}', 'ProductsController@subprods4');
Route::get('products/{subcat}/{subcat2}/{subcat3}/{subcat4}/{subcat5}/{locale}', 'ProductsController@subprods5');
Route::get('about/{locale}', 'AboutController@index');
Route::get('modifyprods/{locale}', 'ProductsController@images');
Route::post('quotation/{locale}', 'QuotationController@new');
Route::get('contact/{locale}', 'ContactController@index');
Route::post('contact/{locale}', 'ContactController@new');
Route::post('bulletin/{locale}', 'ContactController@bulletin');
