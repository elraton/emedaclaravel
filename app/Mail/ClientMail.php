<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ClientMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    public $data;
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $address = 'janeexampexample@example.com';
        $subject = 'This is a demo!';
        $name = 'Jane Doe';
        return $this->view('emails.client')
                    ->from($this->data['fromemail'], $this->data['fromname'])
                    ->replyTo($this->data['fromemail'], $this->data['fromname'])
                    ->subject($this->data['subject'])
                    ->with([ 'mensaje' => $this->data['message'] ]);
        //return $this->view('view.name');
    }
}
