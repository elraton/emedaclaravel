<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mail\ClientMail;
use App\Quotation;
use Mail;

class QuotationController extends Controller
{
    public function new(Request $request, $locale)
    {
        $data = [
            'message' => '<p>Nombre: '.$request->name.'</p><p>Email: '.$request->email.'</p><p>Teléfono: '.$request->phone.'</p><p>Producto: <a href="'.$request->produrl.'">'.$request->prodname.'</a></p>',
            'fromemail' => 'elratonmaton@gmail.com',
            'fromname' => $request->name,
            'subject' => 'Nueva Cotización'
        ];

        $qq = new Quotation;
        $qq->name = $request->name;
        $qq->email = $request->email;
        $qq->phone = $request->phone;
        $qq->product = $request->prodname;

        $qq->save();

        Mail::to('ventas@emedac.com')->send(new ClientMail($data));
        Mail::to('elratonmaton@gmail.com')->send(new ClientMail($data));
    
        return response()->json(['save' => 'ok'], 200);
    }
}

