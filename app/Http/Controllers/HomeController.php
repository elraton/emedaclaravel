<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;

class HomeController extends Controller
{
    public function index($locale)
    {
        return view('home', ['locale' => $locale]);
    }
}
