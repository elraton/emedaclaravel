<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Products;
use App\Categories;

class ProductsController extends Controller
{
    public function index($locale)
    {
        $categories = DB::table('categories')->where('father', 0)->get();
        return view('products', ['locale' => $locale, 'categories' => $categories]);
    }

    public function subprods($subcat, $locale)
    {
        $category = DB::table('categories')->where('slug', $subcat)->first();
        $subcats = DB::table('categories')->orderBy('order')->where('father', $category->id)->get();
        foreach ($subcats as $subcat) {
            $subcat->images = json_decode($subcat->images);
        }
        return view('subproducts', ['locale' => $locale, 'category' => $category, 'subcats' => $subcats]);
    }

    public function subprods2($subcat, $subcat2, $locale)
    {
        $category = DB::table('categories')->where('slug', $subcat)->first();
        $subcat1 = DB::table('categories')->where('slug', $subcat2)->first();
        $subcats = DB::table('categories')->orderBy('order')->where('father', $subcat1->id)->get();
        if ( count($subcats) == 0 ) {
            $subcats = DB::table('products')->orderBy('order')->where('category', $subcat1->id)->get();
        }
        foreach ($subcats as $subcat) {
            $subcat->images = json_decode($subcat->images);
        }
        return view('subproducts2', [
            'locale' => $locale,
            'category' => $category,
            'subcat1' => $subcat1,
            'subcats' => $subcats]);
    }

    public function subprods3($subcat, $subcat2, $subcat3, $locale)
    {
        $category = DB::table('categories')->where('slug', $subcat)->first();
        $subcat_1 = DB::table('categories')->where('slug', $subcat2)->first();
        $subcat_2 = DB::table('categories')->where('slug', $subcat3)->first();
        //$subcats = DB::table('categories')->where('father', $subcat_2->id)->get();

        $subcats = [];
        if ($subcat_2) {
            $subcats = DB::table('categories')->orderBy('order')->where('father', $subcat_2->id)->get();
            if ( count($subcats) == 0 ) {
                $subcats = DB::table('products')->where('category', $subcat_2->id)->get();
            }
            foreach ($subcats as $subcat) {
                $subcat->images = json_decode($subcat->images);
            }
        } else {
            $subcat_2 = DB::table('products')->where('slug', $subcat3)->first();
        }

        return view('subproducts3', [
            'locale' => $locale,
            'category' => $category,
            'subcat1' => $subcat_1,
            'subcat2' => $subcat_2,
            'subcats' => $subcats]);
    }

    public function subprods4($subcat, $subcat2, $subcat3, $subcat4, $locale)
    {
        $category = DB::table('categories')->where('slug', $subcat)->first();
        $subcat_1 = DB::table('categories')->where('slug', $subcat2)->first();
        $subcat_2 = DB::table('categories')->where('slug', $subcat3)->first();
        $subcat_3 = DB::table('categories')->orderBy('order')->where('slug', $subcat4)->first();
        $subcats = [];
        if ($subcat_3) {
            $subcats = DB::table('categories')->orderBy('order')->where('father', $subcat_3->id)->get();
            if ( count($subcats) == 0 ) {
                $subcats = DB::table('products')->where('category', $subcat_3->id)->get();
            }
            foreach ($subcats as $subcat) {
                $subcat->images = json_decode($subcat->images);
            }
        } else {
            $subcat_3 = DB::table('products')->where('slug', $subcat4)->first();
        }
        
        return view('subproducts4', [
            'locale' => $locale,
            'category' => $category,
            'subcat1' => $subcat_1,
            'subcat2' => $subcat_2,
            'subcat3' => $subcat_3,
            'subcats' => $subcats]);
    }

    public function subprods5($subcat, $subcat2, $subcat3, $subcat4, $subcat5, $locale)
    {
        $category = DB::table('categories')->where('slug', $subcat)->first();
        $subcat_1 = DB::table('categories')->where('slug', $subcat2)->first();
        $subcat_2 = DB::table('categories')->where('slug', $subcat3)->first();
        $subcat_3 = DB::table('categories')->orderBy('order')->where('slug', $subcat4)->first();
        
        $subcats = [];
        $subcat_4 = DB::table('products')->where('slug', $subcat5)->first();
        
        
        return view('subproducts5', [
            'locale' => $locale,
            'category' => $category,
            'subcat1' => $subcat_1,
            'subcat2' => $subcat_2,
            'subcat3' => $subcat_3,
            'subcat4' => $subcat_4,
            'subcats' => $subcats
        ]);
    }

    public function images()
    {
        $prods = DB::table('categories')->get();
        foreach ($prods as $pp) {
            $flight = Categories::find($pp->id);
            $flight->images = str_replace("panel/public/", "", $flight->images);
            $flight->save();
        }
        
        return 'lalala';
    }
}
