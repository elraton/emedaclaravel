<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Bulletin;

class ContactController extends Controller
{
    public function index($locale)
    {
        return view('contact', ['locale' => $locale]);
    }

    public function new(Request $request, $locale)
    {
        $data = [
            'message' => '<p>Nombre: '.$request->name.'</p><p>Email: '.$request->email.'</p><p>Teléfono: '.$request->phone.'</p><p>Mensaje: '.$request->message.'</p>',
            'fromemail' => $request->email,
            'fromname' => $request->name,
            'subject' => 'Nuevo Mensaje'
        ];

        Mail::to('informes@emedac.com')->send(new ClientMail($data));
        Mail::to('elratonmaton@gmail.com')->send(new ClientMail($data));
    
        return response()->json(['save' => 'ok'], 200);
    }

    public function bulletin(Request $request, $locale) {
        $bb = new Bulletin;
        $bb->email = $request->email;
        $bb->save();
        return response()->json(['save' => 'ok'], 200);
    }
}
