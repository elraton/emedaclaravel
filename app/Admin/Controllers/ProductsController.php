<?php

namespace App\Admin\Controllers;

use App\Products;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;

class ProductsController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('Index')
            ->description('description')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('Detail')
            ->description('description')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('Edit')
            ->description('description')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('Create')
            ->description('description')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Products);

        $grid->id('Id');
        $grid->name('Name');
        $grid->name_es('Name es');
        $grid->name_pt('Name pt');
        $grid->description('Description');
        $grid->description_es('Description es');
        $grid->description_pt('Description pt');
        $grid->images('Images');
        $grid->father('Father');
        $grid->order('Order');
        $grid->price('Price');
        $grid->category('Category');
        $grid->slug('Slug');
        $grid->created_at('Created at');
        $grid->updated_at('Updated at');

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Products::findOrFail($id));

        $show->id('Id');
        $show->name('Name');
        $show->name_es('Name es');
        $show->name_pt('Name pt');
        $show->description('Description');
        $show->description_es('Description es');
        $show->description_pt('Description pt');
        $show->images('Images');
        $show->father('Father');
        $show->order('Order');
        $show->price('Price');
        $show->category('Category');
        $show->slug('Slug');
        $show->created_at('Created at');
        $show->updated_at('Updated at');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Products);

        $form->text('name', 'Name');
        $form->text('name_es', 'Name es');
        $form->text('name_pt', 'Name pt');
        $form->textarea('description', 'Description');
        $form->textarea('description_es', 'Description es');
        $form->textarea('description_pt', 'Description pt');
        $form->textarea('images', 'Images');
        $form->number('father', 'Father');
        $form->number('order', 'Order');
        $form->decimal('price', 'Price');
        $form->number('category', 'Category');
        $form->text('slug', 'Slug');

        return $form;
    }
}
