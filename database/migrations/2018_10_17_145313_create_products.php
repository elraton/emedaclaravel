<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('name_es');
            $table->string('name_pt');
            $table->text('description');
            $table->text('description_es');
            $table->text('description_pt');
            $table->text('images');
            $table->integer('father')->default(0);
            $table->integer('order')->default(0);
            $table->decimal('price', 10, 2);
            $table->integer('category')->default(0);
            $table->string('slug');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
