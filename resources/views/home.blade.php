@extends('base')
    @section('content')    
        <div class="container-fluid home">
            <div class="row justify-content-center">
                <div class="col-md-12 banner">
                    <div class="slick">
                        <div class="banner-img">
                            <img class="img-fluid" src="/images/banner_cx4.svg" draggable="false">
                            <div class="text-right">
                                <h2>INTENSITY CX4</h2>
                                @if ( $locale == 'en')
                                    <p>The CX4 is an advanced combination of electrotherapy and ultrasound offering the therapist a wide range of treatment options in a single, totally friendly and ergonomically designed equipment. The parameters are easily adjusted or you can choose between the pre-set adjustments according to the treatment to be applied.</p>
                                @endif
                                @if ( $locale == 'es')
                                    <p>La CX4 es una avanzada combinacion de electroterapia y ultrasonido ofreciendo al terapeuta un amplio rango de opciones en tratamientos en un solo equipo totalmente amigable y con diseño ergonomico. Los parametros son ajustados facilmente o elegir entre los ajustes preestablecidos segun el tratamiento a aplicar.</p>
                                @endif
                                @if ( $locale == 'pt')
                                    <p>O CX4 é uma combinação avançada de eletroterapia e ultrassonografia que oferece ao terapeuta uma ampla gama de opções de tratamento em um único equipamento totalmente amigável e ergonomicamente projetado. Os parâmetros são facilmente ajustados ou você pode escolher entre os ajustes pré-definidos de acordo com o tratamento a ser aplicado.</p>
                                @endif
                                <div class="button-cont">
                                    @if ( $locale == 'en')
                                    <a class="button" href="/products/medical-equipment/physical-medicine/combined-therapy/intensity-cx4-clinical-electrotherapy/{{$locale}}">DETAILS</a>
                                    @endif
                                    @if ( $locale == 'es')
                                    <a class="button" href="/products/medical-equipment/physical-medicine/combined-therapy/intensity-cx4-clinical-electrotherapy/{{$locale}}">DETALLES</a>
                                    @endif
                                    @if ( $locale == 'pt')
                                    <a class="button" href="/products/medical-equipment/physical-medicine/combined-therapy/intensity-cx4-clinical-electrotherapy/{{$locale}}">DETALHES</a>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="banner-img">
                            <img class="img-fluid" src="/images/banner_p2500s.svg" draggable="false">
                            <div class="text-right">
                                <h2>POWERSHOCKER LGT-2500S</h2>
                                @if ( $locale == 'en')
                                    <p>The LGT-2500 S is a portable shock wave therapy device. Which can be used by physiotherapists, orthopedists and sports medicine mainly for the treatment of chronic pain in shoulders, back, knees ankles or elbows. It is easy to apply and has a library with pre-established settings for the most frequently used treatments.</p>
                                @endif
                                @if ( $locale == 'es')
                                    <p>EL LGT-2500 S es un equipo de terapia por Ondas de Choque portatil. El cual puede ser utilizao por fisioterapeutas, ortopedistas y en medicina deportiva principalmente para el tratamiento del dolor cronico en hombros, espalda, tobillos rodillas o codos. es de facil aplicacion y posee una libreria con ajustes preestablecidos para los tratamientos utilizados con mayor frecuencia.</p>
                                @endif
                                @if ( $locale == 'pt')
                                    <p>O LGT-2500 S é um dispositivo portátil de terapia por ondas de choque. Que pode ser usado por fisioterapeutas, ortopedistas e medicina esportiva, principalmente para o tratamento da dor crônica nos ombros, costas, joelhos tornozelos ou cotovelos. É fácil de aplicar e tem uma biblioteca com configurações pré-estabelecidas para os tratamentos mais utilizados.</p>
                                @endif
                                <div class="button-cont">
                                    @if ( $locale == 'en')
                                    <a class="button" href="/products/medical-equipment/physical-medicine/shock-waves/powershocker-lgt-2500s/{{$locale}}">DETAILS</a>
                                    @endif
                                    @if ( $locale == 'es')
                                    <a class="button" href="/products/medical-equipment/physical-medicine/shock-waves/powershocker-lgt-2500s/{{$locale}}">DETALLES</a>
                                    @endif
                                    @if ( $locale == 'pt')
                                    <a class="button" href="/products/medical-equipment/physical-medicine/shock-waves/powershocker-lgt-2500s/{{$locale}}">DETALHES</a>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="banner-img">
                            <img class="img-fluid" src="/images/banner_evocombo.svg" draggable="false">
                            <div class="text-right">
                                <h2>RICHMAR EVO COMBO</h2>
                                @if ( $locale == 'en')
                                    <p>Evo Combo is a high performance equipment for medical and clinical centers with a large influx of patients, uses electro stimulation and ultrasound therapy in 4 channels simultaneously for their patients, additionally you can apply laser with this equipment acquiring an additional module.</p>
                                @endif
                                @if ( $locale == 'es')
                                    <p>Evo Combo es un equipo de alto rendimiento para centros medicos y clinicas con gran afluencia de pacientes, utiliza la electro estimulacion y terapia por ultrasonido en 4 canales en simultaneo para sus pacientes, adicionalmente puede aplicar laser con este equipo adquiriendo un modulo adicional.</p>
                                @endif
                                @if ( $locale == 'pt')
                                    <p>O Evo Combo é um equipamento de alto desempenho para centros médicos e clínicos com grande afluência de pacientes, utiliza electroestimulação e terapia de ultra-som em 4 canais simultaneamente para seus pacientes, além de poder aplicar laser com este equipamento adquirindo um módulo adicional.</p>
                                @endif
                                <div class="button-cont">
                                    @if ( $locale == 'en')
                                    <a class="button" href="/products/medical-equipment/physical-medicine/combined-therapy/winner-evo-combo/{{$locale}}">DETAILS</a>
                                    @endif
                                    @if ( $locale == 'es')
                                    <a class="button" href="/products/medical-equipment/physical-medicine/combined-therapy/winner-evo-combo/{{$locale}}">DETALLES</a>
                                    @endif
                                    @if ( $locale == 'pt')
                                    <a class="button" href="/products/medical-equipment/physical-medicine/combined-therapy/winner-evo-combo/{{$locale}}">DETALHES</a>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="banner-img">
                            <img class="img-fluid" src="/images/banner3.svg" draggable="false">
                            <div class="text-right">
                                <h2>HIVAMAT 200</h2>
                                @if ( $locale == 'en')
                                    <p>It has patented technology for rehabilitation treatments using electrostatic energy effects to generate deep oscillations in the muscular tissues to relax them and decrease the pain of the patient.</p>
                                @endif
                                @if ( $locale == 'es')
                                    <p>Posee tecnologia patentada para tratamientos de rehabilitacion utilizando efectos de energia electrostatica para generar oscilaciones profundas en los tejidos musculares para relajarlos y disminuir el dolor del paciente.</p>
                                @endif
                                @if ( $locale == 'pt')
                                    <p>Possui tecnologia patenteada para tratamentos de reabilitação usando efeitos de energia eletrostática para gerar oscilações profundas nos tecidos musculares para relaxá-los e diminuir a dor do paciente.</p>
                                @endif
                                <div class="button-cont">
                                    @if ( $locale == 'en')
                                    <a class="button" href="/products/medical-equipment/physical-medicine/deep-oscillation-therapy/hivamat-200/{{$locale}}">DETAILS</a>
                                    @endif
                                    @if ( $locale == 'es')
                                    <a class="button" href="/products/medical-equipment/physical-medicine/deep-oscillation-therapy/hivamat-200/{{$locale}}">DETALLES</a>
                                    @endif
                                    @if ( $locale == 'pt')
                                    <a class="button" href="/products/medical-equipment/physical-medicine/deep-oscillation-therapy/hivamat-200/{{$locale}}">DETALHES</a>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="banner-img">
                            <img class="img-fluid" src="/images/banner_powerbb.svg" draggable="false">
                            <div class="text-right">
                                <h2>POWERSHOCKER LGT-2510B</h2>
                                @if ( $locale == 'en')
                                    <p>With an integrated design, the main unit is combined with a very useful cart for the use of this equipment. The large touch screen allows the therapist to easily apply a treatment using the pre-established library of the equipment or design a program for a specific patient.</p>
                                @endif
                                @if ( $locale == 'es')
                                    <p>Con un diseño integrado, la unidad principal se encuentra combinada con un carrito de gran utilidad para el uso de este equipo. La gran pantalla tactil permite al terapeuta aplicar de manera sencilla un tratamiento utilizando la librería preestablecida del equipo o diseñar un programa propio para un paciente determinado.</p>
                                @endif
                                @if ( $locale == 'pt')
                                    <p>Com um disenho integrado, a unidade principal é combinada com um carrinho muito útil para o uso deste equipamento. A grande tela sensível ao toque permite ao terapeuta aplicar facilmente um tratamento usando a biblioteca pré-estabelecida do equipamento ou projetar um programa para um paciente específico.</p>
                                @endif
                                <div class="button-cont">
                                    @if ( $locale == 'en')
                                    <a class="button" href="/products/medical-equipment/physical-medicine/shock-waves/powershocker-lgt-2510b/{{$locale}}">DETAILS</a>
                                    @endif
                                    @if ( $locale == 'es')
                                    <a class="button" href="/products/medical-equipment/physical-medicine/shock-waves/powershocker-lgt-2510b/{{$locale}}">DETALLES</a>
                                    @endif
                                    @if ( $locale == 'pt')
                                    <a class="button" href="/products/medical-equipment/physical-medicine/shock-waves/powershocker-lgt-2510b/{{$locale}}">DETALHES</a>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="banner-img">
                            <img class="img-fluid" src="/images/banner-c5pro.svg" draggable="false">
                            <div class="text-right">
                                <h2>ANASONIC C5 PRO</h2>
                                @if ( $locale == 'en')
                                    <p>C5 Pro is a portable, ergonomic and easy-to-use ultrasound device that not only applies to abdominal areas, equipped with a 15 '' screen it has a high precision with dynamic focus for 2D and Doppler images.</p>
                                @endif
                                @if ( $locale == 'es')
                                    <p>C5 Pro es un ecografo portatil, ergonomico y facil de utilizar que no solo se aplica a areas abdominales, equipado una pantalla de 15'' posee una alta presicion con enfoque dinamico para imágenes 2D y Doppler.</p>
                                @endif
                                @if ( $locale == 'pt')
                                    <p>O C5 Pro é um dispositivo de ultrassom portátil, ergonômico e fácil de usar que não se aplica apenas a áreas abdominais, equipado com uma tela de 15 '', possui alta precisão com foco dinâmico para imagens 2D e Doppler.</p>
                                @endif
                                <div class="button-cont">
                                    @if ( $locale == 'en')
                                    <a class="button" href="/products/medical-equipment/diagnostic-by-images/anasonic-c5-pro/{{$locale}}">DETAILS</a>
                                    @endif
                                    @if ( $locale == 'es')
                                    <a class="button" href="/products/medical-equipment/diagnostic-by-images/anasonic-c5-pro/{{$locale}}">DETALLES</a>
                                    @endif
                                    @if ( $locale == 'pt')
                                    <a class="button" href="/products/medical-equipment/diagnostic-by-images/anasonic-c5-pro/{{$locale}}">DETALHES</a>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="banner-img">
                            <img class="img-fluid" src="/images/banner-sc50.svg" draggable="false">
                            <div class="text-right">
                                <h2>ANASONIC SC50</h2>
                                @if ( $locale == 'en')
                                    <p>SC50 ultrasound functional and easy to use is coupled to its own transport cart has a totally elegant design and 18.5-inch LCD screen provide image clarity with more highlight colors and sharper lines.</p>
                                @endif
                                @if ( $locale == 'es')
                                    <p>El ecografo funcional SC50, de facil uso esta acoplado a su propio carrito de transporte posee un diseño totalmente elegante y la pantalla LCD de 18.5 pulgadas brindan una claridad de imagen con colores mas resaltantes y lineas mas nitidas.</p>
                                @endif
                                @if ( $locale == 'pt')
                                    <p>SC50 ecografo funcional e fácil de usar é acoplado ao seu próprio carrinho de transporte tem um design totalmente elegante e tela LCD de 18,5 polegadas fornecem clareza de imagem com mais cores de destaque e linhas mais nítidas.</p>
                                @endif
                                <div class="button-cont">
                                    <a class="button" href="/products/medical-equipment/diagnostic-by-images/anasonic-sc50/{{$locale}}">DETAILS</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row categories">
                <a class="col equipo" href="/products/medical-equipment/{{$locale}}/">
                    <div class="band">
                        <h5>
                        @if ( $locale == 'en')
                            Medical Equipment
                        @endif
                        @if ( $locale == 'es')
                            Equipo Médico
                        @endif
                        @if ( $locale == 'pt')
                            Equipe Médica
                        @endif
                        </h5>
                    </div>
                </a>
                <a class="col accesories" href="/products/medical-accesories/{{$locale}}/">
                    <div class="band">
                        <h5>
                        @if ( $locale == 'en')
                            Medical Accesories
                        @endif
                        @if ( $locale == 'es')
                            Accesorios Médicos
                        @endif
                        @if ( $locale == 'pt')
                            Acessórios Médicos
                        @endif
                        </h5>
                    </div>
                </a>
                <a class="col furniture" href="/products/medical-furniture/{{$locale}}/">
                    <div class="band">
                        <h5>
                        @if ( $locale == 'en')
                            Medical Furniture
                        @endif
                        @if ( $locale == 'es')
                            Mobiliario Médico
                        @endif
                        @if ( $locale == 'pt')
                            Mobiliário Médico
                        @endif
                        </h5>
                    </div>
                </a>
                <a class="col fitness" href="/products/fisiotherapy-and-fitness/{{$locale}}/">
                    <div class="band">
                        <h5>
                        @if ( $locale == 'en')
                            Fisiotherapy and Fitness
                        @endif
                        @if ( $locale == 'es')
                            Fisioterapia y Fitness
                        @endif
                        @if ( $locale == 'pt')
                            Fisioterapia e Fitness
                        @endif
                        </h5>
                    </div>
                </a>
                <a class="col models" href="/products/3d-anatomic-models/{{$locale}}/">
                    <div class="band">
                        <h5>
                        @if ( $locale == 'en')
                            3D Anatomic Models
                        @endif
                        @if ( $locale == 'es')
                            Modelos Anatómicos 3D
                        @endif
                        @if ( $locale == 'pt')
                            Modelos Anatômicos 3D
                        @endif
                        </h5>
                    </div>
                </a>
            </div>
            <div class="row">
                <div class="col-sm-12 subs-cont">
                    <a href="#" class="subscribe">
                        @if ( $locale == 'en')
                            SUBSCRIBE
                        @endif
                        @if ( $locale == 'es')
                            SUSCRÍBETE
                        @endif
                        @if ( $locale == 'pt')
                            SUBSCREVA-SE
                        @endif
                        <span><i class="fas fa-angle-down"></i></span>
                    </a>
                </div>
            </div>
        </div>
    @endsection
    @section('scripts')
        <script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
        <script type="text/javascript">
            $(document).ready(function(){
                $('.slick').slick({
                    infinite: true,
                    speed: 300,
                    slidesToShow: 1,
                    autoplay: true,
                    autoplaySpeed: 3000,
                    prevArrow: '<button type="button" class="slick-prev"><i class="fas fa-angle-left"></i></button>',
                    nextArrow: '<button type="button" class="slick-next"><i class="fas fa-angle-right"></i></button>'
                });

                var h = screen.height - $('.header').height();
                $('.home').height(h);
            });
        </script>
    @endsection
</html>
