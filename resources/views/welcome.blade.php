<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>Emedac</title>

        <link href=" {{ mix('css/app.css') }}" rel="stylesheet">

    </head>
    <body>
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-12 welcome">
                    <a href="/home">
                        <img class="logo" src="/images/logo.png" draggable="false">
                    </a>
                    <h1>@lang('messages.welcome')</h1>
                    <p>@lang('messages.selectlang')</p>
                    <ul class="languages">
                        <li>
                            <a href="/home/en">
                                <img src="/images/english.png" >
                                <span>English</span>
                            </a>
                        </li>
                        <li>
                            <a href="/home/es">
                                <img src="/images/spain.png" >
                                <span>Español</span>
                            </a>
                        </li>
                        <li>
                            <a href="/home/pt">
                                <img src="/images/portuguese.png" >
                                <span>Português</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </body>
</html>