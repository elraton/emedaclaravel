@extends('base')
    @section('content')
        <div class="container about">
            <div class="row justify-content-center">
                <div class="col-md-12 text">
                    <img class="logo" src="/images/logo.png" draggable="false">
                    @if ( $locale == 'en') 
                        <p>We are a Peruvian company specialized in medical and laboratory equipment nationwide distribution, representing international prestige brands in markets such as USA, Germany, Japan.</p>
                        <p>We are committed to provide high quality equipment that is accessible, reliable with excellent post-sales support, providing reliability to health professionals and our final customers.</p>
                        <p>Fisiocorp is leading by a team of engineers focused on the development of new technologies for an evolving health system, being a strategic partner for the Peruvian market of companies like Compass Health Brands, Longest, and Anasonic.</p>
                    @endif
                    @if ( $locale == 'es')
                        <p>Somos una empresa Peruana especializada en la distribución de equipos médicos y de laboratorio a nivel nacional, representando marcas con prestigio internacional en mercados como USA, Alemania, Japón.</p>
                        <p>Nos sentimos comprometidos con brindar equipos de alta calidad que sean accesibles, confiables y con excelente soporte de postventa, brindando confiabilidad a los profesionales de la salud y a nuestros clientes finales.</p>
                        <p>Fisiocorp está liderado por un equipo de ingenieros enfocados en el desarrollo de nuevas tecnologías para un sistema de salud en evolución, siendo un socio estratégico para el mercado peruano de empresas como Compass Health Brands, Longest, y Anasonic.</p>
                    @endif
                    @if ( $locale == 'pt')
                        <p>Somos uma empresa peruana especializada na distribuição de equipamentos médicos e laboratoriais em tudo o país, representando marcas com prestígio internacional em mercados como EUA, Alemanha, Japão.</p>
                        <p>Estamos empenhados em fornecer equipamentos de alta qualidade que é acessível, confiável com excelente suporte pós-venda, proporcionando confiabilidade aos profissionais de saúde e nossos clientes finais.</p>
                        <p>Fisiocorp é liderada por uma equipe de engenheiros focados no desenvolvimento de novas tecnologias para um sistema de saúde em evolução , sendo um parceiro estratégico para o mercado peruano de empresas como Compass Health Brands, Longest e Anasonic.</p>
                    @endif
                </div>
            </div>
        </div>
    @endsection
    @section('scripts')
        <script type="text/javascript">
            $(document).ready(function(){
                var h = screen.height - $('.header').height();
                $('.about').height(h);
            });
        </script>
    @endsection
</html>
