@extends('base')
    @section('content')    
        <div class="container products mb-5">
            <div class="row">
                <div class="col-sm-12 title">
                    @if ( $locale == 'en')<h4>{{ strtolower($category->name) }}</h4>@endif
                    @if ( $locale == 'es')<h4>{{ strtolower($category->name_es) }}</h4>@endif
                    @if ( $locale == 'pt')<h4>{{ strtolower($category->name_pt) }}</h4>@endif
                </div> 
            </div>
            <div class="row historical">
                <div class="col-sm-12 history">
                    <a href="/home/{{ $locale }}"><i class="fas fa-home"></i></a>
                    <span> / </span>
                    <a href="/products/{{ $locale }}">
                        @if ( $locale == 'en') PRODUCTS @endif
                        @if ( $locale == 'es') PRODUCTOS @endif
                        @if ( $locale == 'pt') PRODUTOS @endif
                    </a>
                    <span> / </span>
                    <a href="/products/{{$category->slug}}/{{ $locale }}">
                        @if ( $locale == 'en'){{ strtoupper($category->name) }}@endif
                        @if ( $locale == 'es'){{ strtoupper($category->name_es) }}@endif
                        @if ( $locale == 'pt'){{ strtoupper($category->name_pt) }}@endif
                    </a>
                </div>
            </div>
            <div class="row subprods">
                @foreach ($subcats as $cat)
                    <a href="/products/{{$category->slug}}/{{ $cat->slug }}/{{ $locale }}" class="col-sm-4 product-card">
                        @foreach ($cat->images as $img)
                            @if ($loop->first)
                                <div class="image" style="background: url({{ $img }})"></div>
                            @endif
                        @endforeach
                        @if ( $locale == 'en')<h4>{{ strtolower($cat->name) }}</h4>@endif
                        @if ( $locale == 'es')<h4>{{ strtolower($cat->name_es) }}</h4>@endif
                        @if ( $locale == 'pt')<h4>{{ strtolower($cat->name_pt) }}</h4>@endif
                    </a>
                @endforeach

                
            </div>
        </div>
    @endsection
    @section('scripts')
        <script type="text/javascript">
            $(document).ready(function(){

                var h = screen.height - $('.header').height();
                $('.products').css('min-height', h + 'px');
            });
        </script>
    @endsection
</html>
