@extends('base')
    @section('content')
        <div class="container product mb-5">
            <div class="row historical">
                <div class="col-sm-12 history">
                    <a href="/home/{{ $locale }}"><i class="fas fa-home"></i></a>
                    <span> / </span>
                    <a href="/products/{{ $locale }}">
                        @if ( $locale == 'en') PRODUCTS @endif
                        @if ( $locale == 'es') PRODUCTOS @endif
                        @if ( $locale == 'pt') PRODUTOS @endif
                    </a>
                    <span> / </span>
                    <a href="/products/{{$category->slug}}/{{ $locale }}">
                        @if ( $locale == 'en'){{ strtoupper($category->name) }}@endif
                        @if ( $locale == 'es'){{ strtoupper($category->name_es) }}@endif
                        @if ( $locale == 'pt'){{ strtoupper($category->name_pt) }}@endif
                    </a>
                    <span> / </span>
                    <a href="/products/{{$category->slug}}/{{$subcat1->slug}}/{{ $locale }}">
                        @if ( $locale == 'en'){{ strtoupper($subcat1->name) }}@endif
                        @if ( $locale == 'es'){{ strtoupper($subcat1->name_es) }}@endif
                        @if ( $locale == 'pt'){{ strtoupper($subcat1->name_pt) }}@endif
                    </a>
                    <span> / </span>
                    <a href="/products/{{$category->slug}}/{{$subcat1->slug}}/{{$subcat2->slug}}/{{ $locale }}">
                        @if ( $locale == 'en'){{ strtoupper($subcat2->name) }}@endif
                        @if ( $locale == 'es'){{ strtoupper($subcat2->name_es) }}@endif
                        @if ( $locale == 'pt'){{ strtoupper($subcat2->name_pt) }}@endif
                    </a>
                    <span> / </span>
                    <a href="/products/{{$category->slug}}/{{$subcat1->slug}}/{{$subcat2->slug}}/{{$subcat3->slug}}/{{ $locale }}">
                        @if ( $locale == 'en'){{ strtoupper($subcat3->name) }}@endif
                        @if ( $locale == 'es'){{ strtoupper($subcat3->name_es) }}@endif
                        @if ( $locale == 'pt'){{ strtoupper($subcat3->name_pt) }}@endif
                    </a>
                    <span> / </span>
                    <a href="/products/{{$category->slug}}/{{$subcat1->slug}}/{{$subcat2->slug}}/{{$subcat3->slug}}/{{$subcat4->slug}}/{{ $locale }}">
                        @if ( $locale == 'en'){{ strtoupper($subcat4->name) }}@endif
                        @if ( $locale == 'es'){{ strtoupper($subcat4->name_es) }}@endif
                        @if ( $locale == 'pt'){{ strtoupper($subcat4->name_pt) }}@endif
                    </a>
                </div>
            </div>
            <div class="row">                
                <div class="col-sm-5">
                    <div class="slickprod">
                        @foreach (json_decode($subcat4->images) as $img)
                            <div class="imgcont" style="background: url({{$img}})"></div>
                        @endforeach
                    </div>
                    <div class="slickprodnav">
                        @foreach (json_decode($subcat4->images) as $img)
                            <div class="imgcont" style="background: url({{$img}})"></div>
                        @endforeach
                    </div>
                </div>
                <div class="col-sm-7 text">
                    @if ( $locale == 'en')<h4 class="prodname">{{ strtolower($subcat4->name) }}</h4>@endif
                    @if ( $locale == 'es')<h4 class="prodname">{{ strtolower($subcat4->name_es) }}</h4>@endif
                    @if ( $locale == 'pt')<h4 class="prodname">{{ strtolower($subcat4->name_pt) }}</h4>@endif

                    @if ( $locale == 'en') {!! $subcat4->description !!} @endif
                    @if ( $locale == 'es') {!! $subcat4->description_es !!} @endif
                    @if ( $locale == 'pt') {!! $subcat4->description_pt !!} @endif

                    <div class="buttons">
                        @if ( $locale == 'en')
                            <button data-toggle="modal" data-target="#CotizarModal" type="button" class="btn btn-primary">QUOTE</button>
                            <button type="button" class="btn btn-primary" disabled>BUY</button>
                        @endif
                        @if ( $locale == 'es')
                            <button data-toggle="modal" data-target="#CotizarModal" type="button" class="btn btn-primary">COTIZAR</button>
                            <button type="button" class="btn btn-primary" disabled>COMPRAR</button>
                        @endif
                        @if ( $locale == 'pt')
                            <button data-toggle="modal" data-target="#CotizarModal" type="button" class="btn btn-primary">CITAR</button>
                            <button type="button" class="btn btn-primary" disabled>COMPRAR</button>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <!-- Modal -->
        <div class="modal fade" id="CotizarModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLongTitle">
                                @if ( $locale == 'en')
                                    Quote {{ strtoupper($subcat3->name) }}
                                @endif
                                @if ( $locale == 'es')
                                    Cotizar {{ strtoupper($subcat3->name_es) }}
                                @endif
                                @if ( $locale == 'pt')
                                    Citar {{ strtoupper($subcat3->name_pt) }}
                                @endif
                            
                            </h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form id="modalform">
                                @csrf
                                <div class="form-group">
                                    @if ( $locale == 'en')
                                        <label for="namemodal">Names</label>
                                        <input type="text" class="form-control" id="namemodal" placeholder="John">
                                        <small id="nameHelp" class="form-text text-muted">* You must enter a name.</small>

                                        <label for="emailmodal">Email address</label>
                                        <input type="email" class="form-control" id="emailmodal" placeholder="name@example.com">
                                        <small id="emailHelp" class="form-text text-muted">* You must enter an email.</small>

                                        <label for="phonemodal">Phone</label>
                                        <input type="phone" class="form-control" id="phonemodal" placeholder="phone number">
                                        <small id="phoneHelp" class="form-text text-muted">* You must enter a phone.</small>
                                    @endif
                                    @if ( $locale == 'es')
                                        <label for="namemodal">Nombres</label>
                                        <input type="email" class="form-control" id="namemodal" placeholder="John">
                                        <small id="nameHelp" class="form-text text-muted">* Debe ingresar un nombre.</small>

                                        <label for="emailmodal">Email</label>
                                        <input type="email" class="form-control" id="emailmodal" placeholder="nombre@ejemplo.com">
                                        <small id="emailHelp" class="form-text text-muted">* Debe ingresar un email valido.</small>

                                        <label for="phonemodal">Teléfono</label>
                                        <input type="email" class="form-control" id="phonemodal" placeholder="número de teléfono">
                                        <small id="phoneHelp" class="form-text text-muted">* Debe ingresar un teléfono valido.</small>
                                    @endif
                                    @if ( $locale == 'pt')
                                        <label for="namemodal">Nomes</label>
                                        <input type="email" class="form-control" id="namemodal" placeholder="John">
                                        <small id="nameHelp" class="form-text text-muted">* Debe ingresar un nombre.</small>

                                        <label for="emailmodal">Email</label>
                                        <input type="email" class="form-control" id="emailmodal" placeholder="nome@exemplo.com">
                                        <small id="emailHelp" class="form-text text-muted">* Debe ingresar un email valido.</small>

                                        <label for="phonemodal">Telefone</label>
                                        <input type="email" class="form-control" id="phonemodal" placeholder="número de telefone">
                                        <small id="phoneHelp" class="form-text text-muted">* Debe ingresar un teléfono valido.</small>
                                    @endif
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <img src="/images/Gear-3s-172px.svg" class="loading">
                            @if ( $locale == 'en')
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary sendbutton">Send</button>
                            @endif
                            @if ( $locale == 'es')
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                                <button type="submit" class="btn btn-primary sendbutton">Enviar</button>
                            @endif
                            @if ( $locale == 'pt')
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                                <button type="submit" class="btn btn-primary sendbutton">Enviar</button>
                            @endif
                        </div>
                    </div>
                </div>
            </div>


            <!-- Modal -->
        <div class="modal fade" id="modalMessage" tabindex="-1" role="dialog" aria-labelledby="title" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">
                            @if ( $locale == 'en')
                            Successful quote
                            @endif
                            @if ( $locale == 'es')
                            Cotización realizada con éxito
                            @endif
                            @if ( $locale == 'pt')
                            Citação bem sucedida
                            @endif   
                        </h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <p>
                            @if ( $locale == 'en')
                            Thanks for the quote request, a representative will contact you as soon as possible.
                            @endif
                            @if ( $locale == 'es')
                            Gracias por la solicitud de cotización, un representante se comunicará con ud a la brevedad.
                            @endif
                            @if ( $locale == 'pt')
                            Obrigado pela solicitação de cotação, um representante entrará em contato com você o mais breve possível.
                            @endif
                        </p>
                    </div>
                    <div class="modal-footer"></div>
                </div>
            </div>
        </div>
    @endsection
    @section('scripts')
        <script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
        <script type="text/javascript">
            $(document).ready(function(){
                $('.slickprod').slick({
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    arrows: false,
                    fade: true,
                    asNavFor: '.slickprodnav',
                    infinite: true,
                });

                $('.slickprodnav').slick({
                    slidesToShow: 2,
                    slidesToScroll: 1,
                    asNavFor: '.slickprod',
                    focusOnSelect: true,
                    arrows: false,
                });

                $('.sendbutton').click(function(e){
                    $('.loading').css('display', 'inline');
                    var error = false;
                    if ( $('#namemodal').val() === '' ) {
                        $('#nameHelp').css('display', 'block');
                        $('.loading').css('display', 'none');
                        error = true;
                    } else {
                        $('#nameHelp').css('display', 'none');
                    }
                    if ( $('#emailmodal').val() === '' ) {
                        $('#emailHelp').css('display', 'block');
                        $('.loading').css('display', 'none');
                        error = true;
                    } else {
                        var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
                        if ( !emailReg.test( $('#emailmodal').val() ) ) {
                            $('#emailHelp').css('display', 'block');
                            $('.loading').css('display', 'none');
                            error = true;
                        } else {
                            $('#emailHelp').css('display', 'none');
                        }
                    }
                    if ( $('#phonemodal').val() === '' ) {
                        $('#phoneHelp').css('display', 'block');
                        $('.loading').css('display', 'none');
                        error = true;
                    } else {
                        $('#phoneHelp').css('display', 'none');
                    }

                    if (!error) {
                        var lann = window.location.pathname.split('/');
                        var data = {
                            name: $('#namemodal').val(),
                            email: $('#emailmodal').val(),
                            phone: $('#phonemodal').val(),
                            prodname: $('.prodname').html(),
                            produrl: window.location.href,
                            _token: $('input[name=_token]').val()
                        }
                        $.post( '/quotation/'+lann[lann.length - 1], data, function() {
                            $('#CotizarModal').modal('hide');
                            $('#modalMessage').modal('show');
                            $('.loading').css('display', 'none');
                            $('#namemodal').val('');
                            $('#emailmodal').val('');
                            $('#phonemodal').val('');
                        })
                        .fail(function() {
                            alert( "ha ocurrido un error." );
                        });
                    }
                    
                });

                var h = screen.height - $('.header').height();
                $('.product').css('min-height', h + 'px');
            });
        </script>
    @endsection
</html>
