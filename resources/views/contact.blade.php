@extends('base')
    @section('content')
        <div class="container contact">
            <div class="row justify-content-center content">
                <div class="col-md-12 text">
                    @if ( $locale == 'en')
                        <h4>Contact Us</h4>
                        <p>Our customer service team is trained to clear any questions you have about our products, we invite you to be ready to use. Do not forget that all the fields are obligatory to be able to contact you in your own way.</p>
                    @endif
                    @if ( $locale == 'es')
                        <h4>Contáctenos</h4>
                        <p>Nuestro equipo de servicio al cliente está capacitado para despejar cualquier pregunta que tenga sobre nuestros productos, lo invitamos a que esté listo para usar. no olvide que todos los campos son obligatorios para poder contactarlo de manera propia.</p>
                    @endif
                    @if ( $locale == 'pt')
                        <h4>Contate-nos</h4>
                        <p>Nossa equipe de atendimento ao cliente é treinada para esclarecer quaisquer dúvidas que você tenha sobre nossos produtos, nós o convidamos a estar pronto para usar. Não esqueça que todos os campos são obrigatórios para poder entrar em contato com você do seu jeito.</p>
                    @endif

                    <div class="row justify-content-center mt-5">
                        <div class="col-md-8">
                            <form id="contactform">
                                @csrf
                                <div class="form-group">
                                @if ( $locale == 'en')
                                    <label for="name">Full name</label>
                                    <input type="text" class="form-control" id="name" placeholder="Enter your full name">
                                    <small id="nameHelp" class="form-text text-muted">* You must enter a name.</small>
                                @endif
                                @if ( $locale == 'es')
                                    <label for="name">Nombres</label>
                                    <input type="text" class="form-control" id="name" placeholder="Ingresa tus nombres">
                                    <small id="nameHelp" class="form-text text-muted">* Debe ingresar un nombre.</small>
                                @endif
                                @if ( $locale == 'pt')
                                    <label for="name">Nome completo</label>
                                    <input type="text" class="form-control" id="name" placeholder="Escreva seu nome completo">
                                    <small id="nameHelp" class="form-text text-muted">* Você deve digitar um nome.</small>
                                @endif
                                </div>
                                <div class="form-group">
                                @if ( $locale == 'en')
                                    <label for="email">Email address</label>
                                    <input type="email" class="form-control" id="email" aria-describedby="emailHelp" placeholder="Enter your email">
                                    <small id="emailHelp" class="form-text text-muted">* You must enter an email.</small>
                                @endif
                                @if ( $locale == 'es')
                                    <label for="email">Email</label>
                                    <input type="email" class="form-control" id="email" aria-describedby="emailHelp" placeholder="Ingresa tu email">
                                    <small id="emailHelp" class="form-text text-muted">* Debe ingresar un email valido.</small>
                                @endif
                                @if ( $locale == 'pt')
                                    <label for="email">Email</label>
                                    <input type="email" class="form-control" id="email" aria-describedby="emailHelp" placeholder="Escreva seu email">
                                    <small id="emailHelp" class="form-text text-muted">* Você deve inserir um email válido.</small>
                                @endif
                                </div>
                                <div class="form-group">
                                @if ( $locale == 'en')
                                    <label for="phone">Phone number</label>
                                    <input type="text" class="form-control" id="phone" placeholder="Enter your phone">
                                    <small id="phoneHelp" class="form-text text-muted">* You must enter a phone.</small>
                                @endif
                                @if ( $locale == 'es')
                                    <label for="phone">Teléfono</label>
                                    <input type="text" class="form-control" id="phone" placeholder="Ingresa tu teléfono">
                                    <small id="phoneHelp" class="form-text text-muted">* Debe ingresar un teléfono.</small>
                                @endif
                                @if ( $locale == 'pt')
                                    <label for="phone">Telefone</label>
                                    <input type="text" class="form-control" id="phone" placeholder="Escreva seu telefone">
                                    <small id="phoneHelp" class="form-text text-muted">* Você deve digitar um telefone válido.</small>
                                @endif
                                </div>
                                <div class="form-group">
                                @if ( $locale == 'en')
                                    <label for="message">Message</label>
                                    <textarea class="form-control" id="message" rows="4"></textarea>
                                    <small id="messageHelp" class="form-text text-muted">* You must enter a message.</small>
                                @endif
                                @if ( $locale == 'es')
                                    <label for="message">Mensaje</label>
                                    <textarea class="form-control" id="message" rows="4"></textarea>
                                    <small id="messageHelp" class="form-text text-muted">* Debe ingresar un mensaje.</small>
                                @endif
                                @if ( $locale == 'pt')
                                    <label for="message">Mensagem</label>
                                    <textarea class="form-control" id="message" rows="4"></textarea>
                                    <small id="messageHelp" class="form-text text-muted">* Você deve digitar uma mensagem.</small>
                                @endif
                                </div>
                                
                                <img src="/images/Gear-3s-172px.svg" class="loading">
                                <button id="contactbutton" type="submit" class="btn btn-primary">
                                @if ( $locale == 'en')
                                    Send
                                @endif
                                @if ( $locale == 'es')
                                    Enviar
                                @endif
                                @if ( $locale == 'pt')
                                    Enviar
                                @endif
                                </button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <!-- Modal -->
        <div class="modal fade" id="modalMessage" tabindex="-1" role="dialog" aria-labelledby="title" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">
                            @if ( $locale == 'en')
                            Message sent
                            @endif
                            @if ( $locale == 'es')
                            Mensaje Enviado
                            @endif
                            @if ( $locale == 'pt')
                            Mensagem enviada
                            @endif
                        </h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <p>
                            @if ( $locale == 'en')
                            Thanks for the message, a representative will contact you shortly.
                            @endif
                            @if ( $locale == 'es')
                            Gracias por el mensaje, un representante se comunicará con ud a la brevedad.
                            @endif
                            @if ( $locale == 'pt')
                            Obrigado pela mensagem, um representante entrará em contato com você em breve.
                            @endif
                        </p>
                    </div>
                    <div class="modal-footer"></div>
                </div>
            </div>
        </div>
    @endsection
    @section('scripts')
        <script type="text/javascript">
            $(document).ready(function(e){

                var h = screen.height - $('.header').height();
                $('.contact').height(h);

                $('#contactbutton').click(function(e){
                    e.preventDefault();
                    var name = $('#name').val();
                    var email = $('#email').val();
                    var phone = $('#phone').val();
                    var message = $('#message').val();

                    var error = false;

                    $('.loading').css('display', 'inline');

                    if ( name === '') {
                        error = true;
                        $('#nameHelp').css('display', 'block');
                        $('.loading').css('display', 'none');
                    }

                    if ( email === '') {
                        error = true;
                        $('#emailHelp').css('display', 'block');
                        $('.loading').css('display', 'none');
                    }

                    if ( phone === '') {
                        error = true;
                        $('#phoneHelp').css('display', 'block');
                        $('.loading').css('display', 'none');
                    }

                    if ( message === '') {
                        error = true;
                        $('#messageHelp').css('display', 'block');
                        $('.loading').css('display', 'none');
                    }

                    if (!error) {
                        var lann = window.location.pathname.split('/');
                        var data = {
                            name: $('#name').val(),
                            email: $('#email').val(),
                            phone: $('#phone').val(),
                            message: $('#message').val(),
                            _token: $('input[name=_token]').val()
                        }
                        $.post( '/contact/'+lann[lann.length - 1], data, function() {
                            $('#modalMessage').modal('show');
                            $('.loading').css('display', 'none');
                            $('#name').val('');
                            $('#email').val('');
                            $('#phone').val('');
                            $('#message').val('');
                        })
                        .fail(function() {
                            alert( "ha ocurrido un error." );
                        });
                    }
                });
            });
        </script>
    @endsection
</html>
