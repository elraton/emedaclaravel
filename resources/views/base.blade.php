<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>Emedac</title>

        <link href=" {{ mix('css/app.css') }}" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>

    </head>
    <body>
        <div class="container-fluid header">
            <nav class="navbar navbar-expand-lg navbar-light bg-light">
                <a class="navbar-brand" href="/home/{{$locale}}">
                    <img class="img-fluid" src="/images/logo.png" draggable="false">
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav">
                        <li class="nav-item">
                            <a href="/home/{{ $locale }}" class="nav-link menu-link">
                                @if ( $locale == 'en') HOME @endif
                                @if ( $locale == 'es') INICIO @endif
                                @if ( $locale == 'pt') CASA @endif
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="/products/{{ $locale }}" class="nav-link menu-link">
                                @if ( $locale == 'en') PRODUCTS @endif
                                @if ( $locale == 'es') PRODUCTOS @endif
                                @if ( $locale == 'pt') PRODUTOS @endif
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link menu-link" href="/about/{{ $locale }}">
                                @if ( $locale == 'en') ABOUT US @endif
                                @if ( $locale == 'es') NOSOTROS @endif
                                @if ( $locale == 'pt') SOBRE NÓS @endif
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link menu-link" href="/contact/{{ $locale }}">
                                @if ( $locale == 'en') CONTACT @endif
                                @if ( $locale == 'es') CONTACTO @endif
                                @if ( $locale == 'pt') CONTATO @endif
                            </a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                @if ( $locale == 'en') LANGUAGE @endif
                                @if ( $locale == 'es') IDIOMA @endif
                                @if ( $locale == 'pt') LINGUA @endif
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item chlanguage" href="en">English</a>
                                <a class="dropdown-item chlanguage" href="es">Español</a>
                                <a class="dropdown-item chlanguage" href="pt">Português</a>
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>

        @yield('content')


        <div class="container-fluid">
            <div class="row footer justify-content-center">
                <div class="col-sm-4">
                    <form class="subscribeform">
                        <h5>
                            @if ( $locale == 'en') SUBSCRIBE TO OUR BULLETIN @endif
                            @if ( $locale == 'es') SUSCRÍBETE A NUESTRO BOLETÍN @endif
                            @if ( $locale == 'pt') SUBSCREVA-SE AO NOSSO BOLETIM @endif
                        </h5>
                        @if ( $locale == 'en') 
                            <input class="form-control form-control-lg" type="text" placeholder="Your email">
                        @endif
                        @if ( $locale == 'es') 
                            <input class="form-control form-control-lg" type="text" placeholder="Tu email">
                        @endif
                        @if ( $locale == 'pt')
                            <input class="form-control form-control-lg" type="text" placeholder="Seu email">
                        @endif
                        
                        <button type="submit">
                            @if ( $locale == 'en') SUBSCRIBE @endif
                            @if ( $locale == 'es') SUSCRÍBETE @endif
                            @if ( $locale == 'pt') SUBSCREVA-SE @endif
                        </button>
                    </form>
                    <ul class="social-icons">
                        <li>
                            <a href="#">
                                <i class="fab fa-facebook-f"></i>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="fab fa-twitter-square"></i>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="fab fa-google-plus-g"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <!--<script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>-->
        <script src="/js/jquery-3.2.1.js"></script>
        <script src="/js/popper.js"></script>
        <script src="/js/moment.min.js"></script>
        <script src="/js/bootstrap-datetimepicker.min.js"></script>
        <script src="/js/bootstrap-switch.min.js"></script>
        <script src="/js/bootstrap.min.js"></script>
        <script src="/js/jquery-ui-1.12.1.custom.min.js"></script>
        
        <script src="/js/nouislider.js"></script>
        <script src="/js/paper-kit.js"></script>
        
        
        @yield('scripts')
        <script>
            $('.menu-link').click(function(e) {
                $('.nav-item').removeClass('active');
                $(this).parent().addClass('active');
            });
            $('.menu-link').each(function(){
                if (window.location.pathname === $(this).attr('href')){
                    $(this).parent().addClass('active');
                }
            });

            $('.chlanguage').click(function(e){
                e.preventDefault();
                var lann = window.location.pathname.split('/')
                if ( lann[lann.length - 1] !== $(this).attr('href') ) {
                    lann[lann.length - 1] = $(this).attr('href');
                    window.location.href = lann.join('/');
                } else {
                    return;
                }
            });
        </script>
    </body>
</html>