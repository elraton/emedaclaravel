@extends('base')
    @section('content')    
        <div class="container products mb-5">
            <div class="row">
                <div class="col-sm-12 title">
                    <h4>
                        @if ( $locale == 'en')
                            OUR PRODUCTS
                        @endif
                        @if ( $locale == 'es')
                            NUESTROS PRODUCTOS
                        @endif
                        @if ( $locale == 'pt')
                            NOSSOS PRODUTOS
                        @endif
                    </h4>
                </div> 
            </div>
            <div class="row cont-prod">
                <a class="col-sm-5 product-card" href="/products/medical-equipment/{{$locale}}/">
                    <div class="row">
                        <div class="col-sm-7 card-text">
                            <img class="card-icon" src="/images/medicalequipment.svg" draggable="false">
                            @if ( $locale == 'en')
                                <h6 class="card-title">Medical Equipment</h6>
                                <p class="card-subtitle">Physical Medicine</p>
                                <p class="card-subtitle">Diagnostic by images</p>
                            @endif
                            @if ( $locale == 'es')
                                <h6 class="card-title">Equipo Médico</h6>
                                <p class="card-subtitle">Medicina Física</p>
                                <p class="card-subtitle">Diagnostico por imágenes</p>
                            @endif
                            @if ( $locale == 'pt')
                                <h6 class="card-title">Equipe Médica</h6>
                                <p class="card-subtitle">Medicina Física</p>
                                <p class="card-subtitle">Diagnóstico por imágem</p>
                            @endif
                        </div>
                        <div class="col-sm-5 card-image" style="background: url('/images/EquipoMedico.jpg')">
                            <div class="mask"></div>
                        </div>
                    </div>
                </a>

                <a class="col-sm-5 product-card" href="/products/medical-accesories/{{$locale}}/">
                    <div class="row">
                        <div class="col-sm-7 card-text">
                            <img class="card-icon" src="/images/medicalaccesories.svg" draggable="false">
                            @if ( $locale == 'en')
                                <h6 class="card-title">Medical Accesories</h6>
                                <p class="card-subtitle">Elements of Evaluation</p>
                            @endif
                            @if ( $locale == 'es')
                                <h6 class="card-title">Accesorios Médicos</h6>
                                <p class="card-subtitle">Elementos de Evaluación</p>
                            @endif
                            @if ( $locale == 'pt')
                            <h6 class="card-title">Acessórios Médicos</h6>
                                <p class="card-subtitle">Elementos de Avaliação</p>
                            @endif
                        </div>
                        <div class="col-sm-5 card-image" style="background: url('/images/instrumental-medico.jpg')">
                            <div class="mask"></div>
                        </div>
                    </div>
                </a>

                <a class="col-sm-5 product-card mt-3" href="/products/medical-furniture/{{$locale}}/">
                    <div class="row">
                        <div class="col-sm-7 card-text">
                            <img class="card-icon" src="/images/furniture.svg" draggable="false">
                            @if ( $locale == 'en')
                                <h6 class="card-title">Medical Furniture</h6>
                                <p class="card-subtitle">Beds of hospitalization</p>
                                <p class="card-subtitle">Auxiliary furniture</p>
                                <p class="card-subtitle">Physiotherapy</p>
                            @endif
                            @if ( $locale == 'es')
                                <h6 class="card-title">Mobiliario Médico</h6>
                                <p class="card-subtitle">Camas de hospitalización</p>
                                <p class="card-subtitle">Mobiliario auxiliar</p>
                                <p class="card-subtitle">Fisioterapia</p>
                            @endif
                            @if ( $locale == 'pt')
                                <h6 class="card-title">Mobiliário Médico</h6>
                                <p class="card-subtitle">Camas de hospitalização</p>
                                <p class="card-subtitle">Móveis auxiliares</p>
                                <p class="card-subtitle">Fisioterapia</p>
                            @endif
                        </div>
                        <div class="col-sm-5 card-image" style="background: url('/images/mobiliario.jpg')">
                            <div class="mask"></div>
                        </div>
                    </div>
                </a>

                <a class="col-sm-5 product-card mt-3" href="/products/fisiotherapy-and-fitness/{{$locale}}/">
                    <div class="row">
                        <div class="col-sm-7 card-text">
                            <img class="card-icon" src="/images/fitness.svg" draggable="false">
                            @if ( $locale == 'en')
                                <h6 class="card-title">Fisiotherapy and Fitness</h6>
                                <p class="card-subtitle">Fisiotherapy</p>
                                <p class="card-subtitle">Fitness</p>
                            @endif
                            @if ( $locale == 'es')
                                <h6 class="card-title">Fisioterapia y Fitness</h6>
                                <p class="card-subtitle">Fisioterapia</p>
                                <p class="card-subtitle">Fitness</p>
                            @endif
                            @if ( $locale == 'pt')
                                <h6 class="card-title">Fisioterapia e Fitness</h6>
                                <p class="card-subtitle">Fisioterapia</p>
                                <p class="card-subtitle">Fitness</p>
                            @endif
                            
                        </div>
                        <div class="col-sm-5 card-image" style="background: url('/images/bandas.jpg')">
                            <div class="mask"></div>
                        </div>
                    </div>
                </a>

                <a class="col-sm-5 product-card mt-3" href="/products/3d-anatomic-models/{{$locale}}/">
                    <div class="row">
                        <div class="col-sm-7 card-text">
                            <img class="card-icon" src="/images/anatomic.svg" draggable="false">
                            @if ( $locale == 'en')
                                <h6 class="card-title">3D Anatomic Models</h6>
                                <p class="card-subtitle">Whole body</p>
                                <p class="card-subtitle">Head and thorns</p>
                                <p class="card-subtitle">Superior limbs</p>
                                <p class="card-subtitle">Lower limbs</p>
                            @endif
                            @if ( $locale == 'es')
                                <h6 class="card-title">Modelos Anatómicos 3D</h6>
                                <p class="card-subtitle">Cuerpo completo</p>
                                <p class="card-subtitle">Cabeza y espina</p>
                                <p class="card-subtitle">Miembros superiores</p>
                                <p class="card-subtitle">Miembros inferiores</p>
                            @endif
                            @if ( $locale == 'pt')
                            <h6 class="card-title">Modelos Anatômicos 3D</h6>
                            <p class="card-subtitle">Corpo completo</p>
                            <p class="card-subtitle">Cabeça e espinhos</p>
                            <p class="card-subtitle">Membros superiores</p>
                            <p class="card-subtitle">Membros inferiores</p>
                            @endif
                        </div>
                        <div class="col-sm-5 card-image" style="background: url('/images/models.jpg')">
                            <div class="mask"></div>
                        </div>
                    </div>
                </a>
            </div>
        </div>
    @endsection
    @section('scripts')
        <script type="text/javascript">
            $(document).ready(function(){

                var h = screen.height - $('.header').height();
                $('.products').css('min-height', h + 'px');
            });
        </script>
    @endsection
</html>
