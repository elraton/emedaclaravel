
/*require('./bootstrap');

window.Vue = require('vue');

Vue.component('welcome', require('./components/welcome.vue'));
Vue.component('home', require('./components/home.vue'));

const app = new Vue({
    el: '#app'
});*/




import Vue from 'vue'
import VueRouter from 'vue-router'
require('./bootstrap');

Vue.use(VueRouter)

import welcome from './components/welcome'
import home from './components/home'
import products from './components/products'
import headervue from './components/headervue'
import footervue from './components/footervue'

const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'welcome',
            component: welcome
        },
        {
            path: '/home',
            name: 'home',
            component: home,
        },
        {
            path: '/products',
            name: 'products',
            component: products,
        },
    ],
});

const app = new Vue({
    el: '#app',
    data: {
        currentRoute: window.location.pathname
    },
    components: { welcome, headervue, footervue },
    router,
});