<?php

return [
    'welcome' => 'bem-vindo a EMEDAC',
    'selectlang' => 'Por favor selecione uma língua',
    'menu-home' => 'CASA',
    'menu-products' => 'PRODUTOS',
    'menu-about' => 'SOBRE NÓS',
    'menu-contact' => 'CONTATO',
    'menu-language' => 'LÍNGUA',
];