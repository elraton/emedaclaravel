<?php

return [
    'welcome' => 'Bienvenido a EMEDAC',
    'selectlang' => 'Por favor seleccione un idioma',
    'menu-home' => 'INICIO',
    'menu-products' => 'PRODUCTOS',
    'menu-about' => 'QUIENES SOMOS',
    'menu-contact' => 'CONTACTO',
    'menu-language' => 'IDIOMA',
];