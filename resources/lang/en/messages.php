<?php

return [
    'welcome' => 'Welcome to EMEDAC',
    'selectlang' => 'Please select a language',
    'menu-home' => 'HOME',
    'menu-products' => 'PRODUCTS',
    'menu-about' => 'ABOUT US',
    'menu-contact' => 'CONTACT',
    'menu-language' => 'LANGUAGE',
];